package ru.dm_dev.communication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.util.Log;

public class ReceiveMessageActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String KEY = "keyValue";

    private TextView textView = null;
    private EditText editText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_message);

        textView = (TextView) findViewById(R.id.text_view);
        editText = (EditText) findViewById(R.id.edit_text);

        Button sendButton = (Button) findViewById(R.id.btn_send_back);
        sendButton.setOnClickListener(this);

        Intent intent = getIntent();
        if (intent != null) {
            textView.setText(intent.getStringExtra(KEY));
        }
    }

    private void sendMessageBack() {
        Intent intent = new Intent();
        intent.putExtra("message", editText.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_send_back) {
            Log.d("ReceiveMessageActivity", "sendMessageBack button pressed");
//            Intent intent = new Intent();
//            intent.putExtra("name", editText.getText().toString());
//            setResult(RESULT_OK, intent);
//            finish();
            sendMessageBack();
        } else {
            Log.e("ReceiveMessageActivity", "Unknown view Id on OnClickListener");
        }
    }
}
