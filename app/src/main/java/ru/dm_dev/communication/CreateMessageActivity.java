package ru.dm_dev.communication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreateMessageActivity extends AppCompatActivity {

    private static final int SEND_MESSAGE_BACK = 1;
    private EditText editText = null;
    private TextView textView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);
        editText = (EditText) findViewById(R.id.editText);
        textView = (TextView) findViewById(R.id.text_back);
    }

    public void onSendMessage(View view) {
//        Intent intent = new Intent(this, ReceiveMessageActivity.class);
//        intent.putExtra(ReceiveMessageActivity.KEY, editText.getText().toString());
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(ReceiveMessageActivity.KEY, editText.getText().toString());
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, SEND_MESSAGE_BACK);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("CreateMessageActivity","onActivityResult executed");
        if (requestCode == SEND_MESSAGE_BACK) {
            if (resultCode == RESULT_OK) {
                textView.setText(String.format(getResources().getString(R.string.msq_answer), data.getStringExtra("message")));
            } else {
                textView.setText(getResources().getString(R.string.msq_answer_null));
            }
        }
    }
}
